package WDC043_S2_A2;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PrimeNumber {
    public static void main(String[] args){

        int[] primeNum = new int[5];
        primeNum[0] = 2;
        primeNum[1] = 3;
        primeNum[2] = 5;
        primeNum[3] = 7;
        primeNum[4] = 11;

        System.out.println("The third prime number is: " + primeNum[2]);


        ArrayList<String> names = new ArrayList<>(Arrays.asList("John","Jane","Chloe","Zoey"));
        System.out.println("My friends are: "+ names);

        HashMap<String, Integer> items = new HashMap<String, Integer>()
                {
                    {
                        put("toothbrush", 15);
                        put("toothpaste", 20);
                        put("soap", 12);
                    }

                };

            System.out.println("Our current inventory consists of: "+items);

    }

}
